class Rectangulo:
    def __init__(self,b,a):
        self.base = b
        self.altura = a

    def calculo_area(self):
        return (self.base * self.altura)

    def calculo_perimetro(self):
        return (self.base * 2) + (self.altura * 2)
    def __str__(self):
        return "El perimetro del rectangulo es {} y el area es {}".format(self.calculo_perimetro(), self.calculo_area())
    

rectangulo1= Rectangulo(1,2)
print(rectangulo1)

